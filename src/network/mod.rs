use crate::network::layer::{Layer, SoftmaxLayer, SigmoidLayer, ReluLayer, Neuron};
use std::fs::File;
use std::io::Write;

pub mod layer;

pub enum LayerType {
	Softmax,
	Sigmoid,
	RELU,
}

pub enum ErrorType {
	MSE, //mean squared error
	BinaryCrossEntropy,
	MultiCrossEntropy,
}

pub struct Network {
	layers: Vec<Box<dyn Layer>>,
	debug: File,
}

fn mse(outputs: &Vec<f32>, expected: usize) ->f32 {
	let mut error: f32 = 0.0;
	let mean = outputs.len() as f32;
	let len = outputs.len();

	for i in 0..len {
		let mut prop = 0.0f32;
		if i == expected {
			prop = 1.0;
		}
		let sum = outputs[i] - prop;
		error += sum.powf(2.0);
	}


	error/mean
}

fn mse_derive(output: f32, expected: f32, total: f32) -> f32 {
	2.0*(output-expected)/total
}

impl Network {
	pub fn new() -> Network {
		let mut file = File::create("output.csv").unwrap();
		let mut n = Network {
			layers: Vec::new(),
			debug: file,
		};
		n
	}

	pub fn add_layer(&mut self, inputs: i32, size: i32, l_type: LayerType) {
		match l_type {
			LayerType::Softmax => self.layers.push(Box::new(SoftmaxLayer::new(size, inputs))),
			LayerType::Sigmoid => self.layers.push(Box::new(SigmoidLayer::new(size, inputs))),
			LayerType::RELU => self.layers.push(Box::new(ReluLayer::new(size, inputs))),
		}
	}

	pub fn predict_from_output(&mut self, input: &Vec<f32>) -> i32 {

		self.activate(&input);
		let outputs = self.layers.last().unwrap().get_outputs();


		let mut prediction = 0;
		let mut max = -5000000000.0f32;
		let len = outputs.len();

		for i in 0..len {
			let sum = outputs[i];
			if sum > max {
				prediction = i;
				max = sum;
			}
		}

		prediction as i32
	}

	pub fn debug_init(&mut self, inputs: usize) {
		let mut len = inputs;

		self.debug.write_fmt(format_args!("Error, ")).unwrap();

		for i in 0..len {
			self.debug.write_fmt(format_args!("Input {}, ", i)).unwrap();
		}

		len = self.layers.len();
		for i in 0..len {
			self.layers[i].debug_init(&mut self.debug, (i+1) as i32);
		}

		self.debug.write(b"\n");
	}

	fn error(&self, error_type: ErrorType, expected: usize) -> f32{
		match error_type{
			ErrorType::MSE => {
				return mse(&self.layers.last().unwrap().get_outputs(), expected)
			},
			default => {
				return -1.0
			}
		};
	}

	pub fn debug(&mut self, input: &Vec<f32>, error_type: ErrorType, expected: usize) {

		let mut len = input.len();
		let error = self.error(error_type, expected);
		self.debug.write_fmt(format_args!("{}, ", error)).unwrap();

		for i in 0..len {
			self.debug.write_fmt(format_args!("{}, ", input[i])).unwrap();
		}


		len = self.layers.len();
		for i in 0..len {
			self.layers[i].debug(&mut self.debug);
		}
		self.debug.write(b"\n");
	}

	fn activate(&mut self, initial_inputs: &Vec<f32>) {
		let len = self.layers.len();
		let mut input;
		for i in 0..len {
			if i != 0 {
				input = self.layers[i-1].get_outputs();
				self.layers[i].process(input);
			} else {
				self.layers[i].process(initial_inputs.clone());
			}

		}
	}

	pub fn back_propagate(&mut self, input: &Vec<f32>, expected: usize, e_type: ErrorType) -> Vec<Vec<Neuron>>{
		let mut holder = Vec::new();
		//get initial error
		let mut output_error = Vec::new();
		let mut len = self.layers.last().unwrap().get_outputs().len();

		//fill the neural network with correct information
		self.activate(input);

		//get initial set of output errors
		let output = self.layers.last().unwrap().get_outputs();
		for i in 0..len {
			let error;

			match e_type {
				ErrorType::MSE => {
					error = mse_derive(output[i], expected as f32, len as f32);
				}
				default=> return holder
			}

			output_error.push(error);
		}

		len = self.layers.len();

		for i in (0..len).rev() {
			if i == 0 {
				self.layers[i].propagate(&output_error, input);
			} else {
				let previous_outputs = self.layers[i-1].get_outputs();
				output_error = self.layers[i].propagate(&output_error, &previous_outputs);
			}

		}

		for j in 0..len {
			let neuron_length = self.layers[j].get_outputs().len();
			holder.push(self.layers[j].clone_neurons());
		}


		holder
	}

	pub fn apply_correction(&mut self, corrections: &Vec<Vec<Neuron>>, learning: f32, mean: usize) {
		let quantity = corrections.len();

		for i in 0..quantity {
			self.layers[i].apply_correction(&corrections[i], mean, learning);
		}

	}


}