use rand::prelude::*;
use rand_distr::{StandardNormal};
use std::f32::consts::E;
use std::fs::File;
use std::io::Write;


pub trait Layer {
	fn get_size(&self) -> i32;
	fn get_outputs(&self) -> Vec<f32>;
	fn process(&mut self, inputs: Vec<f32>);
	fn propagate(&mut self, output_error: &Vec<f32>, inputs: &Vec<f32>) -> Vec<f32>;
	fn clone_neurons(&self) -> Vec<Neuron>;
	fn apply_correction(&mut self, corrections:  &Vec<Neuron>, quantity: usize, learning: f32);
	fn debug_init(&self, file: &mut File, layer: i32);
	fn debug(&self, file: &mut File);
}

#[derive(Clone)]
pub struct Neuron {
	pub weights: Vec<f32>,
	pub weight_errors: Vec<f32>,
	pub bias: f32,
	pub bias_errors: f32,
	pub near_output: f32,
	pub near_output_error: f32,
	pub output: f32,
}

impl Neuron {

	fn new(inputs: i32, )  -> Neuron {
		let mut neuron = Neuron {
			weights: Vec::new(),
			weight_errors: Vec::new(),
			bias: 0.0,
			bias_errors: 0.0,
			near_output: 0.0,
			near_output_error: 0.0,
			output: 0.0,
		};
		neuron.set_standard_values(inputs);
		neuron
	}

	fn set_standard_values(&mut self, inputs: i32) {
		for _i in 0..inputs {
			self.weights.push(thread_rng().sample(StandardNormal));
			//println!("{}",self.weights.last().unwrap());
		}
		self.bias = thread_rng().sample(StandardNormal);
	}

	fn activate(&mut self, inputs: &Vec<f32>) {
		if inputs.len() != self.weights.len() {
			println!("Cannot activate, inputs don't match weights");
			return;
		}
		let len = self.weights.len();
		let mut sum :f32 = 0.0;

		for i in 0..len {
			sum += self.weights[i] * inputs[i];
		}

		sum += self.bias;

		self.near_output = sum;
	}

	fn set_output(&mut self, output: f32) {
		self.output = output;
	}

	fn back_propgate(&mut self, output_error: f32, near_output_error: f32, inputs: &Vec<f32>) {
		self.near_output_error = near_output_error;
		self.bias_errors = output_error*near_output_error;
		self.weight_errors = Vec::new();
		let len = self.weights.len();
		for i in 0..len {
			self.weight_errors.push( output_error*near_output_error*inputs[i]);
		}
	}

	fn apply_correction(&mut self, correction: &Neuron, quantity: usize, learning: f32) {
		let len = self.weights.len();
		for i in 0..len {
			let to_change = (learning*correction.weight_errors[i]) / quantity as f32;
			self.weights[i] -= to_change;
		}
		let to_change = (learning*correction.bias_errors);
		self.bias -= to_change;
	}

}

pub struct SoftmaxLayer {
	neurons: Vec<Neuron>,
}

impl SoftmaxLayer {
	pub fn new(size: i32, inputs: i32) -> SoftmaxLayer {

		let mut neurons = Vec::new();

		for _i in 0..size {
			neurons.push(Neuron::new(inputs));
		}

		SoftmaxLayer{
			neurons
		}
	}

	fn exp_sum(&self) -> f32 {
		let mut sum: f32 = 0.0;
		let len = self.neurons.len();

		for i in 0..len {
			sum += E.powf(self.neurons[i].near_output);
		}

		sum
	}

	fn softmax_activation(&mut self) {

		let len = self.neurons.len();
		let sum = self.exp_sum();

		for i in 0..len {
			let near_output = self.neurons[i].near_output;
			self.neurons[i].set_output(E.powf(near_output)/sum);
			//println!("{}", self.neurons[i].output);
		}

	}

}

impl Layer for SoftmaxLayer {
	fn get_size(&self) -> i32 {
		self.neurons.len() as i32
	}

	fn get_outputs(&self) -> Vec<f32> {
		let mut outputs = Vec::new();
		let len = self.neurons.len();
		for i in 0..len {
			outputs.push(self.neurons[i].output);
		}

		outputs
	}

	fn process(&mut self, inputs: Vec<f32>) {

		let len = self.neurons.len();

		for i in 0..len {
			self.neurons[i].activate(&inputs);
		}

		self.softmax_activation();
	}

	fn propagate(&mut self, output_error: &Vec<f32>, inputs: &Vec<f32>) -> Vec<f32> {
		unimplemented!()
	}

	fn clone_neurons(&self) -> Vec<Neuron> {
		unimplemented!()
	}

	fn apply_correction(&mut self, corrections: &Vec<Neuron>, quantity: usize, learning: f32) {
		unimplemented!()
	}

	fn debug_init(&self, file: &mut File, layer: i32) {
		let len = self.neurons.len();

		for i in 0..len {
			let input_len = self.neurons[i].weights.len();
			for j in 0..input_len {
				file.write_fmt(format_args!("Layer {} Neuron {} Weight {}, ", layer, i, j)).unwrap();
			}
			file.write_fmt(format_args!("Layer {} Neuron {} Bias, ", layer, i)).unwrap();
			file.write_fmt(format_args!("Layer {} Neuron {} Output, ", layer, i)).unwrap();
		}
	}

	fn debug(&self, file: &mut File) {
		let len = self.neurons.len();

		for i in 0..len {
			let input_len = self.neurons[i].weights.len();
			for j in 0..input_len {
				file.write_fmt(format_args!("{}, ", self.neurons[i].weights[j])).unwrap();
			}
			file.write_fmt(format_args!("{}, ", self.neurons[i].bias)).unwrap();
			file.write_fmt(format_args!("{}, ", self.neurons[i].output)).unwrap();
		}
	}
}

pub struct SigmoidLayer {
	neurons: Vec<Neuron>,
}

impl SigmoidLayer {

	#[allow(dead_code)]
	pub fn new(size: i32, inputs: i32) -> SigmoidLayer {
		let mut neurons = Vec::new();

		for _i in 0..size {
			neurons.push(Neuron::new(inputs));
		}
		SigmoidLayer {
			neurons,
		}
	}

	fn sigmoid_activation(&mut self) {
		let len = self.neurons.len();

		for i in 0..len {
			let near_output = self.neurons[i].near_output;
			self.neurons[i].set_output(1.0 / (1.0 + E.powf(near_output)) );
		}
	}
}

impl Layer for SigmoidLayer {
	fn get_size(&self) -> i32 {
		self.neurons.len() as i32
	}

	fn get_outputs(&self) -> Vec<f32> {
		let mut outputs = Vec::new();
		let len = self.neurons.len();
		for i in 0..len {
			outputs.push(self.neurons[i].output);
		}

		outputs
	}

	fn process(&mut self, inputs: Vec<f32>) {
		let len = self.neurons.len();

		for i in 0..len {
			self.neurons[i].activate(&inputs);
		}

		self.sigmoid_activation();
	}

	fn propagate(&mut self, output_error: &Vec<f32>, inputs: &Vec<f32>) -> Vec<f32> {
		let neuron_len = self.neurons.len();
		let len = self.neurons.len();

		for i in 0..len {
			let near_output_error = -1.0 * self.neurons[i].output * (1.0 - self.neurons[i].output );
			self.neurons[i].back_propgate(output_error[i], near_output_error, inputs);
		}

		//get errors to return;
		let mut h_errors = Vec::new();
		let input_len = inputs.len();

		for j in 0..input_len {
			let mut sum = 0.0;
			for i in 0..len {
				let near_output_error = 1.0 * ((self.neurons[i].output>1.0) as i32 )as f32;
				sum += inputs[j] * near_output_error * output_error[i];
			}
			h_errors.push(sum);

		}


		return h_errors
	}

	fn clone_neurons(&self) -> Vec<Neuron> {
		self.neurons.clone()
	}

	fn apply_correction(&mut self, corrections: &Vec<Neuron>, quantity: usize, learning: f32) {
		let len = self.neurons.len();
		for i in 0..len {
			self.neurons[i].apply_correction(&corrections[i], quantity, learning);
		}
	}

	fn debug_init(&self, file: &mut File, layer: i32) {
		let len = self.neurons.len();

		for i in 0..len {
			let input_len = self.neurons[i].weights.len();
			for j in 0..input_len {
				file.write_fmt(format_args!("Layer {} Neuron {} Weight {}, ", layer, i, j)).unwrap();
			}
			file.write_fmt(format_args!("Layer {} Neuron {} Bias, ", layer, i)).unwrap();
			file.write_fmt(format_args!("Layer {} Neuron {} Output, ", layer, i)).unwrap();
		}
	}

	fn debug(&self, file: &mut File) {
		let len = self.neurons.len();

		for i in 0..len {
			let input_len = self.neurons[i].weights.len();
			for j in 0..input_len {
				file.write_fmt(format_args!("{}, ", self.neurons[i].weights[j])).unwrap();
			}
			file.write_fmt(format_args!("{}, ", self.neurons[i].bias)).unwrap();
			file.write_fmt(format_args!("{}, ", self.neurons[i].output)).unwrap();
		}
	}
}

pub struct ReluLayer {
	neurons: Vec<Neuron>,
}

impl ReluLayer {

	#[allow(dead_code)]
	pub fn new(size: i32, inputs: i32) -> ReluLayer {
		let mut neurons = Vec::new();

		for _i in 0..size {
			neurons.push(Neuron::new(inputs));
		}

		ReluLayer {
			neurons
		}
	}

	fn relu_activation(&mut self) {
		let len = self.neurons.len();
		for _i in 0..len {
			let near_output = self.neurons[_i].near_output;

			let equ;

			if near_output > 0.0 {
				equ = near_output;
			} else {
				equ = 0.0;
			}

			self.neurons[_i].output = equ;
		}
	}

}

impl Layer for ReluLayer {
	fn get_size(&self) -> i32 {
		self.neurons.len() as i32
	}

	fn get_outputs(&self) -> Vec<f32> {
		let mut outputs = Vec::new();
		let len = self.neurons.len();
		for i in 0..len {
			outputs.push(self.neurons[i].output);
		}

		outputs
	}

	fn process(&mut self, inputs: Vec<f32>) {
		let len = self.neurons.len();

		for i in 0..len {
			self.neurons[i].activate(&inputs);
		}
		self.relu_activation();
	}

	fn propagate(&mut self, output_error: &Vec<f32>, inputs: &Vec<f32>) -> Vec<f32> {

		let neuron_len = self.neurons.len();
		let len = self.neurons.len();

		for i in 0..len {
			let near_output_error = 1.0 * ((self.neurons[i].output>1.0) as i32 )as f32;
			self.neurons[i].back_propgate(output_error[i], near_output_error, inputs);
		}

		//get errors to return;
		let mut h_errors = Vec::new();
		let input_len = inputs.len();

		for j in 0..input_len {
			let mut sum = 0.0;
			for i in 0..len {
				let near_output_error = 1.0 * ((self.neurons[i].output>1.0) as i32 )as f32;
				sum += inputs[j] * near_output_error * output_error[i];
			}
			h_errors.push(sum);

		}


		return h_errors
	}

	fn clone_neurons(&self) -> Vec<Neuron> {
		self.neurons.clone()
	}

	fn apply_correction(&mut self, corrections: &Vec<Neuron>, quantity: usize, learning: f32) {
		let len = self.neurons.len();
		for i in 0..len {
			self.neurons[i].apply_correction(&corrections[i], quantity, learning);
		}
	}

	fn debug_init(&self, file: &mut File, layer: i32) {
		let len = self.neurons.len();

		for i in 0..len {
			let input_len = self.neurons[i].weights.len();
			for j in 0..input_len {
				file.write_fmt(format_args!("Layer {} Neuron {} Weight {}, ", layer, i, j)).unwrap();
			}
			file.write_fmt(format_args!("Layer {} Neuron {} Bias, ", layer, i)).unwrap();
			file.write_fmt(format_args!("Layer {} Neuron {} Output, ", layer, i)).unwrap();
		}

	}

	fn debug(&self, file: &mut File) {
		let len = self.neurons.len();

		for i in 0..len {
			let input_len = self.neurons[i].weights.len();
			for j in 0..input_len {
				file.write_fmt(format_args!("{}, ", self.neurons[i].weights[j])).unwrap();
			}
			file.write_fmt(format_args!("{}, ", self.neurons[i].bias)).unwrap();
			file.write_fmt(format_args!("{}, ", self.neurons[i].output)).unwrap();
		}

	}
}
