extern crate rand;
extern crate rand_distr;

use crate::network::Network;
use crate::network::LayerType::{RELU, Softmax, Sigmoid};
use crate::network::ErrorType::{MSE, BinaryCrossEntropy, MultiCrossEntropy};
use crate::network::layer::Neuron;

pub mod network;

fn main() {

    let mut n  = Network::new();
    n.add_layer(2,4,Sigmoid);
    //n.add_layer(2,2,RELU);
    n.add_layer(4,2,Sigmoid);



    let mut inputs :Vec<Vec<f32>> = Vec::new();
    inputs.push(vec![0.0,0.0]);
    inputs.push(vec![0.0,1.0]);
    inputs.push(vec![1.0,0.0]);
    inputs.push(vec![1.0,1.0]);

    n.debug_init(inputs[1].len());

    let answers: Vec<usize> = vec![1,0,0,1];
    let mut len = inputs.len();
    let mut correct = 0;

    let mut correction_holder : Vec<Vec<Vec<Neuron>>> = Vec::new();

    n.predict_from_output(&inputs[3]);

    for i in 0..len*6 {
        correction_holder.push(n.back_propagate(&inputs[3], answers[3], MSE));
    }

    n.debug(&inputs[3], MSE, answers[3]);
    len = correction_holder.len();
    for i in 0..len {
        n.apply_correction(&correction_holder[i], 0.1, len);
        n.predict_from_output(&inputs[3]);
        n.debug(&inputs[3], MSE, answers[3]);
    }





    //n.back_propagate(&inputs[0], answers[0], MSE);

    println!("Number correct");
}
